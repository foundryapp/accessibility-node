.PHONY: all swift cpp

all: swift cpp

swift:
	swift build --package-path SwiftAccessibility
	rm lib//libSwiftAccessibility.dylib
	cp ./SwiftAccessibility/.build/debug/libSwiftAccessibility.dylib lib/libSwiftAccessibility.dylib

cpp:
	g++ -Wall -std=c++11 -Wl,-rpath,'@loader_path/lib' -L@loader_path/lib -lSwiftAccessibility ./cpp/*.cc -o cpp/build/accessibility


