{
  "targets": [
    {
      "target_name": "accessibility_node",
      "cflags!": [
        "-fno-exceptions"
      ],
      "cflags_cc!": [
        "-fno-exceptions"
      ],
      "sources": [
        "cpp/main.cc",
        "cpp/accessibility.cc"
      ],
      "include_dirs": [
        "<!@(node -p \"require('node-addon-api').include\")"
      ],
      "link_settings": {
        "libraries": [
          "-Wl,-rpath,@loader_path/../../lib/"
        ]
      },
      "libraries": [
        "<(module_root_dir)/lib/libSwiftAccessibility.dylib",
      ],
      "dependencies": [
        "<!(node -p \"require('node-addon-api').gyp\")"
      ],
      "defines": [
        "NAPI_DISABLE_CPP_EXCEPTIONS"
      ]
    }
  ]
}