## Installation & usage
`yarn add @foundryapp/accessibility-node`

### Initialization
```javascript
const ax = require('@foundryapp/accessibility-node'); // ax means accessibility
```

## Methods

### `isProcessTrusted() => boolean`
```javascript
// isProcessTrusted() returns boolean indicating whether a process has accessibility permissions.
// If the process isn't trusted, the method will also prompt user to give this process accessibility permissions.
const isTrusted = ax.isProcessTrusted();
```

### `isAppRunning(bundleID: boolean) => boolean`
```javascript
const isRunning = ax.isAppRunning('com.apple.Terminal');
```

### `launchApp(bundleID: string, (err) => void)`
```javascript
// The exact behavior of the launch depends on a target app but usually it means
// that also a new window of that app is launched. So there's no need to call
// createNewWindow().
ax.launchApp('com.apple.Terminal', (err) => {
  if (!err) {
    console.log('App successfully launched');
  } else {
    console.error(err);
  }
});
```

### `createNewWindow(bundleID: string, (err) => void)`
```javascript
// App must be running, otherwise error is passed to a callback.
// This might take a few seconds to complete.
ax.createNewWindow('com.apple.Terminal', (err) => {
  if (!err) {
    console.log('Window created');
  } else {
    console.error(err);
  }
});
```




## TODO
- Add Typescript typings
