#include <napi.h>

class AXErrorAsyncWorker : public Napi::AsyncWorker {
  public:
      AXErrorAsyncWorker(
          const Napi::Function& callback,
          Napi::Error error
      ) : Napi::AsyncWorker(callback), error(error)
      {
      }

      ~AXErrorAsyncWorker() {}

  protected:
    void Execute() override
    {
      // Do nothing.
    }

    void OnOK() override
    {
      Napi::Env env = Env();

      Callback().MakeCallback(
          Receiver().Value(),
          {
            error.Value(),
            env.Undefined()
          }
      );
    }

    void OnError(const Napi::Error& e) override
    {
      Napi::Env env = Env();

      Callback().MakeCallback(
          Receiver().Value(),
          {
            e.Value(),
            env.Undefined()
          }
      );
    }

  private:
    Napi::Error error;
};
