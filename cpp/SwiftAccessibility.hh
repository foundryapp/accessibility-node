// typedef void (FocusAppCB)(const char* error);
typedef void (LaunchAppCB)(const char* error);
typedef void (CreateNewWindowCB)(const char* error);

extern "C"
{
  bool swift_IsProcessTrusted();
  void swift_Run(const char* bundleID);
  // void swift_FocusApp(const char* bundleID, FocusAppCB callback);
  void swift_LaunchApp(const char* bundleID, LaunchAppCB callback);
  void swift_CreateNewWindow(const char* bundleID, CreateNewWindowCB callback);
  bool swift_IsAppRunning(const char* bundleID);
}
