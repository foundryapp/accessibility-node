#include <napi.h>

using namespace Napi;

namespace accessibility {
  // TODO: Are non-wrapper methods needed here?
  // void FocusApp(const char* bundleID);
  // void LaunchApp(const char* bundleID);

  Boolean IsProcessTrustedWrapper(const CallbackInfo& info);
  // Value FocusAppWrapper(const CallbackInfo& info);
  void LaunchAppWrapper(const CallbackInfo& info);
  void CreateNewWindowWrapper(const CallbackInfo& info);
  void RunWrapper(const CallbackInfo& info);
  Value IsAppRunningWrapper(const CallbackInfo& info);
  Object Init(Env env, Object exports);
}
