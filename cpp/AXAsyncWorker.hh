#include <napi.h>
#include <iostream>
#include <thread>
#include <chrono>
#include <atomic>

class AXAsyncWorker : public Napi::AsyncWorker {
  public:
    AXAsyncWorker(const Napi::Function& callback)
    : Napi::AsyncWorker(callback),
      canFinish(false) {}

    ~AXAsyncWorker() {}

    std::atomic<bool> canFinish;
    std::string error;

  void Execute() override {
    while (!canFinish.load()) {
      // Wait.
      // std::this_thread::sleep_for(std::chrono::milliseconds(5));
    }
  };
  void OnOK() override {
    Napi::Env env = Env();

    Callback().MakeCallback(
        Receiver().Value(),
        {
          error.empty() ? env.Null() : Napi::Error::New(env, error).Value(),
        }
    );
  };
  void OnError(const Napi::Error& e) override {
    // Napi::Env env = Env();
    Callback().MakeCallback(
        Receiver().Value(),
        {
          e.Value(),
        }
     );
  }
};

