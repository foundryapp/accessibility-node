#include <napi.h>
#include "accessibility.hh"

Napi::Object InitAll(Napi::Env env, Napi::Object exports) {
  return accessibility::Init(env, exports);
}

NODE_API_MODULE(accessibility_node, InitAll)


