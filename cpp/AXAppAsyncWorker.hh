#include <napi.h>
#include <iostream>
#include "SwiftAccessibility.hh"

class AXAppAsyncWorker : public Napi::AsyncWorker {
  public:
    AXAppAsyncWorker(
        const Napi::Function& callback,
        const std::string bid)
    : Napi::AsyncWorker(callback),
      bundleID(bid) {}

    ~AXAppAsyncWorker() {}

   protected:
    void Execute() override {
      char* c_bundleID = new char [bundleID.length()+1];
      strcpy(c_bundleID, bundleID.c_str());

      swift_Run(c_bundleID);
    };

    void OnOK() override {
      // Napi::Env env = Env();

      Callback().MakeCallback(
          Receiver().Value(),
          {}
      );
    };
    void OnError(const Napi::Error& e) override {
      // Napi::Env env = Env();
      Callback().MakeCallback(
          Receiver().Value(),
          {
            e.Value(),
          }
       );
    }

  private:
    std::string bundleID;
};

