// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SwiftAccessibility",
    products: [
        .executable(name: "SwiftAccessibilityExec", targets: ["SwiftAccessibility"]),
        .library(name: "SwiftAccessibility", type: .dynamic, targets: ["SwiftAccessibility"]),
    ],
    dependencies: [
        .package(url: "https://github.com/mlejva/AXSwift", from: "0.2.4"),
    ],
    targets: [
        .target(
            name: "SwiftAccessibility",
            dependencies: ["AXSwift"]),
    ]
)
