import XCTest
@testable import SwiftAccessibility

final class SwiftAccessibilityTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(SwiftAccessibility().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
