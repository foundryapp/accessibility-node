import XCTest

import SwiftAccessibilityTests

var tests = [XCTestCaseEntry]()
tests += SwiftAccessibilityTests.allTests()
XCTMain(tests)
