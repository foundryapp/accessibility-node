const ax = require('../build/Release/accessibility_node.node');
const { promisify } = require('util');

function launch() {
  return new Promise((resolve, reject) => {
    ax.launchApp('com.apple.Terminal', (err, arg1, arg2) => {
      console.log('Callback from JavaScript', err, arg1, arg2);
      resolve();
    });
  })
}

function window() {
}

const delay = promisify(setTimeout);
const newWindow = promisify(ax.createNewWindow);

(async function() {
  // await launch();
  console.log(process.pid)
  // const cw = promisify(ax.createNewWindow);
  // await cw('com.apple.Terminal');

  /*
  ax.createNewWindow('com.apple.Terminal', (err) => {
    console.log('CALLBACK', err);
  });
  */

  if (ax.isAppRunning('com.apple.Terminal')) {
    console.log('APP IS RUNNING');
    await newWindow('com.apple.Terminal');
  } else {
    console.log('APP IS NOT RUNNING');
    await launch();
  }
})()


/*
ax.createNewWindow('com.apple.Terminal', (err) => {
  console.error(err);
});
*/
// console.log(ax)
// ax.run('com.apple.Terminal', () => console.log('App finished running'));
setTimeout(() => console.log('exiting...'), 10 * 1000)

